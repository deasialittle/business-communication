//
//  helloWorld.cpp
//  
//
//  Created by Deasia Little on 2/2/17.
//
//

#include "helloWorld.hpp"
// A hello world program in C++

#include<iostream>
using namespace std;

int main()
{
    cout << "Hello World!";
    return 0;
}
